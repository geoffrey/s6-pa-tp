#include <stdio.h>
#include <stdlib.h>
#define SIZE 20000

int main() {
    int i, j;

    // 2.1.3
    int *v;
    v = (int *) malloc(SIZE * sizeof(int));
    for (i = 0; i < SIZE; i++) {
        v[i] = i;
    }
    // Affichage pour vérifier
    // for (i = 0; i < SIZE; i++) {
    //     // printf("v[%d] = %d\n", i, v[i]);
    // }

    // 2.1.4
    int **mat;
    mat = (int **) malloc(SIZE * sizeof(int));
    for (i = 0; i < SIZE; i++) {
        v = (int *) malloc(SIZE * sizeof(int));
        // On attenint la limite pour une taille égale à la RAM + le SWAP disponible
        if (v == NULL) {
            printf("Limite atteinte: i = %d\n", i);
            return EXIT_FAILURE;
        }
        mat[i] = v;
        for (j = 0; j < SIZE; j++) {
            v[j] = i + j;
        }
    }
    // Affichage pour vérifier
    // for (i = 0; i < SIZE; i++) {
    //     for (j = 0; j < SIZE; j++) {
    //         printf("mat[%d][%d] = %d\n", i, j, mat[i][j]);
    //     }
    // }

    // 2.1.6
    for (i = 0; i < SIZE; i++) {
        free(mat[i]);
    }
    free(mat);

    return EXIT_SUCCESS;
}
