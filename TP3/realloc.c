#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

// 2.2.1 isspace vérifie si son argument est un caractère d'espacement
// Elle est responsable du fait que le programme n'imprime que le premier mot envoyé

// 2.2.2
int main() {
    int i, taille_chaine = 1, taille_vect = 16;
    char *chaine = malloc(taille_vect * sizeof(char));
    char *chaine_temp;
    char c;
    c = getchar();

    while (! isspace(c)) {
        taille_chaine++;
        if (taille_chaine > taille_vect) {
            taille_vect += 8;
            chaine_temp = malloc(taille_vect * sizeof(char));
            for (i = 0; i < taille_chaine; i++) {
                chaine_temp[i] = chaine[i];
            }
            free(chaine);
            chaine = chaine_temp;
        }
        chaine[taille_chaine-1] = c;
        c = getchar();
    }

    for (i = 0; i < taille_chaine; i++) {
        putchar(chaine[i]);
    }
    free(chaine);

    putchar('\n');

    return 0;
}

// 2.2.4
int main4() {
    int i, taille_chaine = 1, taille_vect = 16;
    char *chaine = malloc(taille_vect * sizeof(char));
    char c;
    c = getchar();

    while (! isspace(c)) {
        taille_chaine++;
        if (taille_chaine > taille_vect) {
            taille_vect += 8;
            chaine = realloc(chaine, taille_vect * sizeof(char));
        }
        chaine[taille_chaine-1] = c;
        c = getchar();
    }

    for (i = 0; i < taille_chaine; i++) {
        putchar(chaine[i]);
    }
    free(chaine);

    putchar('\n');

    return 0;
}
