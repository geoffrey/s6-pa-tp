#define SIZE 27500
int M[SIZE][SIZE];

int main() {
    // 2.1.1
    // Sur une machine de TP Astruc ça plante à partir de SIZE ≥ 1448,
    // ce qui correspond à SIZE²×sizeof(int)/1024 = 1448²×4/1024 = 8190 KiB
    int i, j;
    for (i = 0; i < SIZE; i++) {
        for (j = 0; j < SIZE; j++) {
            M[i][j] = i + j;
        }
    }

    // 2.2.2
    // Avec une definition de M en globale, ça plante à partir de 27500 < SIZE ≤ 30000
    // Ça dépend en fait de la quantité de RAM et de SWAP disponible

}
