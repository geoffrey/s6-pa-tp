#include<stdlib.h>
#include<stdio.h>
#include<ctype.h>

#define CHAINE_ALLOC_INCREMENT 8

// 3.1
typedef struct {
    char* data;
    int alloc;
    int size;
} Chaine;

// 3.2
void init_chaine(Chaine* chaine) {
    chaine->data = NULL;
    chaine->alloc = 0;
    chaine->size = 0;
}

// 3.3
void clean_chaine(Chaine* chaine) {
    free(chaine->data);
    // On remet tout à zéro au cas où quelqu'un veuille réutiliser la chaine
    // qu'il n'essaye pas d'accéder à des emplacement mémoire inexistants
    init_chaine(chaine);
}

// 3.4
void print_chaine(Chaine* chaine) {
    int i;
    for (i = 0; i < chaine->size; i++) {
        putchar(chaine->data[i]);
    }
}

// 3.5
void concat_chaine_char(Chaine* chaine, char c) {
    chaine->size++;
    if (chaine->size > chaine->alloc) {
        chaine->alloc += CHAINE_ALLOC_INCREMENT;
        chaine->data = realloc(chaine->data, chaine->alloc * sizeof(char));
    }
    chaine->data[chaine->size-1] = c;
}

// 3.7
void concat_chaine_chaine(Chaine* ch1, Chaine* ch2) {
    if (ch1->size + ch2->size > ch1->alloc) {
        ch1->alloc = ch1->size + ch2->size;
        ch1->data = realloc(ch1->data, ch1->alloc * sizeof(char));
    }
    int i;
    for (i = 0; i < ch2->size; i++) {
        ch1->data[ch1->size+i] = ch2->data[i];
    }
    ch1->size += ch2->size;
}

// Remarque : Ici le '\0' n'est pas géré, et on a size <= alloc

int main() {
    // 3.6
    Chaine chaine;
    init_chaine(&chaine);
    char c;
    c = getchar();
    while (! isspace(c)) {
        concat_chaine_char(&chaine, c);
        c = getchar();
    }
    print_chaine(&chaine);
    putchar('\n');

    // 3.7
    Chaine chaine2;
    init_chaine(&chaine2);
    c = getchar();
    while (! isspace(c)) {
        concat_chaine_char(&chaine2, c);
        c = getchar();
    }
    concat_chaine_chaine(&chaine, &chaine2);
    print_chaine(&chaine);
    putchar('\n');

    clean_chaine(&chaine);
    clean_chaine(&chaine2);
    return EXIT_SUCCESS;
}

