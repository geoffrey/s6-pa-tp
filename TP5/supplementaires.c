#include<stdlib.h>
#include<stdio.h>
#include<stdbool.h>

struct Cellule {
    struct Cellule* suiv;
    int val;
    int mult;
};
typedef struct Cellule Cellule;

void ajout_tete(Cellule** l, int val, int mult) {
    Cellule* p;
    p = malloc(sizeof(Cellule));
    p->val = val;
    p->suiv = *l;
    p->mult = mult;
    *l = p;
}

void imprimer(Cellule* l) {
    while (l != NULL) {
        printf("%d × %d\n", l->val, l->mult);
        l = l->suiv;
    }
}

void supprimer_tete(Cellule** l) {
    Cellule* p;
    p = (*l)->suiv;
    free(*l);
    *l = p;
}

void inserer(Cellule** p, int val) {
    Cellule* l = *p;
    if (l == NULL) {
        ajout_tete(p, val, 1);
    } else if (l->val == val) {
        l->mult++;
    } else if (l->val > val) {
        ajout_tete(p, val, 1);
    } else {
        inserer(&l->suiv, val);
    }
}

bool est_trie(Cellule* l) {
    if (l == NULL || l->suiv == NULL) {
        return true;
    }
    if (l->val < l->suiv->val) {
        return est_trie(l->suiv);
    } else {
        return false;
    }
}

void desallouer(Cellule* l) {
    if (l == NULL) {
        return;
    }
    desallouer(l->suiv);
    free(l);
}

int multiplicite(Cellule* l, int val) {
    if (l == NULL) {
        return 0;
    }
    if (l->val == val) {
        return l->mult;
    } else {
        return multiplicite(l->suiv, val);
    }
}

int cardinal(Cellule* l) {
    if (l == NULL) {
        return 0;
    }
    return l->mult + cardinal(l->suiv);
}

// Pour simplifier l'affichage
void details(Cellule *l, char nom) {
    printf("\nListe %c :\n", nom);
    imprimer(l);
    if (est_trie(l)) {
        printf("→ Est triée\n");
    } else {
        printf("→ N'est pas triée\n");
    }
}

Cellule* union_me(Cellule* j, Cellule* k) {
    Cellule* u;
    int i;
    if (j == NULL) {
        if (k == NULL) {
            return NULL;
        } else {
            u = union_me(NULL, k->suiv);
            ajout_tete(&u, k->val, k->mult);
            return u;
        }
    } else {
        u = union_me(j->suiv, k);
        for (i = 0; i < j->mult; i++) {
            inserer(&u, j->val);
        }
        return u;
    }
}

int min(int a, int b) {
    if (a > b) {
        return b;
    } else {
        return a;
    }
}

Cellule* intersection_me(Cellule* j, Cellule* k) {
    if (j == NULL || k == NULL) {
        return NULL;
    } else if (j->val == k->val) {
        Cellule* i;
        i = intersection_me(j->suiv, k->suiv);
        ajout_tete(&i, j->val, min(j->mult, k->mult));
        return i;
    } else if (j->val < k->val) {
        return intersection_me(j->suiv, k);
    } else {
        return intersection_me(j, k->suiv);
    }
}

Cellule* difference_me(Cellule* j, Cellule* k) {
    Cellule* i;
    if (k == NULL && j == NULL) {
        i = NULL;
    } else if (j == NULL) {
        i = difference_me(NULL, k->suiv);
        ajout_tete(&i, k->val, - k->mult);
    } else if (k == NULL) {
        i = difference_me(j->suiv, NULL);
        ajout_tete(&i, j->val, j->mult);
    } else if (j->val == k->val) {
        i = difference_me(j->suiv, k->suiv);
        ajout_tete(&i, j->val, j->mult - k->mult);
    } else if (j->val < k->val) {
        i = difference_me(j->suiv, k);
        ajout_tete(&i, j->val, j->mult);
    } else {
        i = difference_me(j, k->suiv);
        ajout_tete(&i, k->val, - k->mult);
    }
    return i;
}


int main() {
    Cellule* j = NULL;
    inserer(&j, 3);
    inserer(&j, 2);
    inserer(&j, 3);
    inserer(&j, 2);
    inserer(&j, 1);
    inserer(&j, 3);
    details(j, 'j');

    Cellule* k = NULL;
    inserer(&k, 3);
    inserer(&k, 3);
    inserer(&k, 2);
    inserer(&k, 5);
    inserer(&k, 5);
    inserer(&k, 5);
    details(k, 'k');

    Cellule* u = union_me(j, k);
    details(u, 'u');

    Cellule* i = intersection_me(j, k);
    details(i, 'i');

    Cellule* d = difference_me(j, k);
    details(d, 'd');

}
