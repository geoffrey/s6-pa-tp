#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#include<string.h>

#include "listechaines.h"

//Affichage de la liste en ligne
void afficher_liste(Liste l) {
    while (l != NULL) {
        printf("%s\n", l->val);
        l = l->suiv;
    }
}

//Ajout d'un mot en tete de la liste
void ajout_tete(Liste *l, char *mot) {
    Cellule* p;
    p = malloc(sizeof(Cellule));
    strncpy(p->val, mot, MAXSIZE);
    p->suiv = *l;
    *l = p;
}

//Suppression du mot en tete de la liste
void supp_tete(Liste *l) {
    Cellule * p;
    p = (*l)->suiv;
    free(*l);
    *l = p;
}

//Ajout un mot dans une liste supposee
// triee dans l'ordre alphabetique
void ajout_alphab(Liste *p, char *mot) {
    Cellule* l = *p;
    if (l == NULL) {
        ajout_tete(p, mot);
        return;
    }
    while (l->suiv != NULL && l->val < mot) {
        p = &l->suiv;
        l = l->suiv;
    }
    int ret = strcmp(l->val, mot);
    if (ret == 0) {
        return;
    } else if (ret < 0) {
        p = &l->suiv;
    }
    ajout_tete(p, mot);
}

//Dit si un mot donne est dans la liste
//pas forcement triee
bool appartient(Liste l ,char *mot) {
    while (l != NULL) {
        if (strcmp(l->val, mot) == 0) {
            return true;
        }
        l = l->suiv;
    }
    return false;
}

//Donne la taille de la liste.
int taille(Liste l) {
    int taille = 0;
    while (l != NULL) {
        taille++;
        l = l->suiv;
    }
    return taille;
}

//construit une liste triee a partir d'un fichier
void charge_fichier(FILE * fp, Liste *l) {
    char mot[MAXSIZE];
    while (fscanf(fp, "%s", mot) != EOF) {
        ajout_alphab(l, mot);
    }
}

//Destruction de Liste.
void detruire_liste(Liste* l) {
    Cellule* next;
    Cellule* current = *l;
    while (current != NULL) {
        next = current->suiv;
        free(current);
        current = next;
    }
}

