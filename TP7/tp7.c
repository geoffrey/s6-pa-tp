#include <stdlib.h>
#include <stdio.h>
#include <listechaines.h>

int main(int argc, char* argv[]) {
    Liste l = NULL;

    if (argc == 2) {
        printf("Chargement du fichier proposé.\n");
        printf("(pour charger la liste intégrée, utilisez : %s)\n", argv[0]);
        FILE *fp;
        fp = fopen(argv[1], "r");
        if (fp == NULL) {
            printf("Impossible d'ouvrir le fichier %s.\n", argv[1]);
            return EXIT_FAILURE;
        }
        charge_fichier(fp, &l);
    } else {
        printf("Chargement de la liste intégrée.");
        printf("(pour charger votre propre liste, utilisez : %s FICHIER_ENTREE)\n", argv[0]);

        ajout_alphab(&l, "allez");
        ajout_alphab(&l, "vous");
        ajout_alphab(&l, "comment");
        ajout_alphab(&l, "Bonjour");
        ajout_alphab(&l, "?");
    }


    supp_tete(&l);

    printf("\n");
    afficher_liste(l);
    printf("\n");

    if (appartient(l, "Bonjour")) {
        printf("La liste a dit Bonjour.\n");
    } else {
        printf("La liste n'a pas dit Bonjour.\n");
    }

    printf("La liste contient %d éléments.\n", taille(l));

    detruire_liste(&l);

    return EXIT_SUCCESS;
}
