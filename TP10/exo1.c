#include <stdlib.h>
#include <stdio.h>

// 2.1.1
// Signature : int f(int)
int fois_deux(int a) {
    return a*2;
}

// 2.1.2
void appliquer_tableau(int f(int), int t[], int size) {
    int i;
    for (i = 0; i < size; i++) {
        t[i] = f(t[i]);
    }
}

// 2.1.3
#define SIZE 5

int main() {
    int t[SIZE] = {1, 4, 7, 9, 3};
    int i;

    appliquer_tableau(fois_deux, t, SIZE);

    for (i = 0; i < SIZE; i++) {
        printf("t[%d] = %d\n", i, t[i]);
    }
}
