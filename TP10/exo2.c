#include <stdlib.h>
#include <stdio.h>


// 2.2.1
void triBulleOld(int tab[], int size){
    int i, j, tmp;
    for(i = size - 1; i > 0; i--){
        for(j = 0; j < i; j++){
            // 2.2.2
            // Il faut modifier le < en > pour faire un tri décroissant
            if (tab[j+1] < tab[j]){
                tmp = tab[j+1];
                tab[j+1] = tab[j];
                tab[j] = tmp;
            }
        }
    }
}

// 2.2.3
int superieur(int a, int b) {
    if (a > b) {
        return 1;
    } else if (a == b) {
        return 0;
    } else {
        return -1;
    }
}

// 2.2.4
int inferieur(int a, int b) {
    if (a < b) {
        return 1;
    } else if (a == b) {
        return 0;
    } else {
        return -1;
    }
}

// 2.2.5
void triBulle(int tab[], int size, int compare(int, int)) {
    int i, j, tmp;
    for(i = size - 1; i > 0; i--){
        for(j = 0; j < i; j++){
            // 2.2.2
            // Il faut modifier le < en > pour faire un tri décroissant
            if (compare(tab[j+1], tab[j]) == 1){
                tmp = tab[j+1];
                tab[j+1] = tab[j];
                tab[j] = tmp;
            }
        }
    }
}

#define SIZE 5

void afficherTableau(int t[], int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("t[%d] = %d\n", i, t[i]);
    }
}

int main() {
    int t[SIZE] = {1, 4, 7, 9, 3};

    printf("Tri avec inferieur\n");
    triBulle(t, SIZE, inferieur);
    afficherTableau(t, SIZE);

    printf("Tri avec superieur\n");
    triBulle(t, SIZE, superieur);
    afficherTableau(t, SIZE);

}
