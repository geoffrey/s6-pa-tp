#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define TAILLE_CHAINE 100
#define MAX_PERSONNES 10

typedef struct {
    int j, m, a;
} Date;

typedef struct {
    char nom[TAILLE_CHAINE];
    char prenom[TAILLE_CHAINE];
    char telephone[TAILLE_CHAINE];
    Date naissance;
} Personne;

typedef struct {
    Personne personnes[MAX_PERSONNES];
    int nb;
} Annuaire;

Date lire_date() {
    Date d;
    printf("Jour ? ");
    scanf("%d", &d.j);
    printf("Mois ? ");
    scanf("%d", &d.m);
    printf("Année ? ");
    scanf("%d", &d.a);
    return d;
}

void affiche_date(Date d) {
    printf("%2d/%2d/%4d\n", d.j, d.m, d.a);
}

int lire_personne(Personne* p) {
    printf("Nom ? ");
    int res = scanf("%s", p->nom);
    if (res == EOF) {
        return 1;
    }
    printf("Prénom ? ");
    scanf("%s", p->prenom);
    printf("Date de naissance ?\n");
    p->naissance = lire_date();
    printf("N° de téléphone ? ");
    scanf("%s", p->telephone);
    return 0;
}

void affiche_personne(Personne p) {
    printf("Prénom : %s\n", p.prenom);
    printf("Nom : %s\n", p.nom);
    printf("Téléphone : %s\n", p.telephone);
    printf("Naissance :");
    affiche_date(p.naissance);
}

void construire_annuaire(Annuaire* a) {
    int i, res;
    for (i = 0; i < MAX_PERSONNES; i++) {
       // res = lire_personne(&(*a)[i]);
       res = lire_personne(&a->personnes[i]);
       if (res != 0) {
           a->nb = i;
           break;
       }
    }
}

void afficher_annuaire(Annuaire* a) {
    int i;
    for (i = 0; i < a->nb; i++) {
       affiche_personne(a->personnes[i]);
    }
}

char compare_dates(Date d1, Date d2) {
    if (d1.j == d2.j && d1.m == d2.m && d1.a == d2.a) {
        return 0;
    } else if (d1.a > d2.a || (d1.a == d2.a && (d1.m > d2.m || (d1.m == d2.m && d1.j > d2.j)))) {
        return 1;
    } else {
        return -1;
    }
}

char compare_nom(Personne p1, Personne p2) {
    int nomComp = strcmp(p1.nom, p2.nom);
    if (nomComp == 0) {
        return strcmp(p1.prenom, p2.prenom);
    } else {
        return nomComp;
    }
}

void triDate(Annuaire* annuaire) {
    int i, j, trie;
    Personne temp;
    for (i = annuaire->nb - 1; i >= 1; i--) {
        trie = 1;
        for (j = 0; j <= i - 1; j++) {
            if (compare_dates(annuaire->personnes[j+1].naissance, annuaire->personnes[j].naissance) < 0) {
                temp = annuaire->personnes[j+1];
                annuaire->personnes[j+1] = annuaire->personnes[j];
                annuaire->personnes[j] = temp;
                trie = 0;
            }
        }
        if (trie) {
            return;
        }
    }
}

void triNom(Annuaire* annuaire) {
    int i, j, trie;
    Personne temp;
    for (i = annuaire->nb - 1; i >= 1; i--) {
        trie = 1;
        for (j = 0; j <= i - 1; j++) {
            if (compare_nom(annuaire->personnes[j+1], annuaire->personnes[j]) < 0) {
                temp = annuaire->personnes[j+1];
                annuaire->personnes[j+1] = annuaire->personnes[j];
                annuaire->personnes[j] = temp;
                trie = 0;
            }
        }
        if (trie) {
            return;
        }
    }
}

int main() {

    // Date d;
    // d = lire_date();
    // affiche_date(d);

    // Personne d;
    // lire_personne(&d);
    // affiche_personne(d);

    Annuaire a;
    construire_annuaire(&a);

    printf("\n# Pas de tri\n");
    afficher_annuaire(&a);

    printf("\n# Trié par nom\n");
    triNom(&a);
    afficher_annuaire(&a);

    printf("\n# Trié par date\n");
    triDate(&a);
    afficher_annuaire(&a);

    return EXIT_SUCCESS;
}


