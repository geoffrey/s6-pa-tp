#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAXNUM 12

/* Pour compiler, rajoutez l'argument "-lm" a l'appel de gcc  */

int main()
{
  // index était un char, et a va de -128 à 128
  // cependant on doit l'utiliser pour des valeurs beaucoup plus grandes
  // il y avait donc overflow
  int index;
  char * tableau;
  double taille = exp(MAXNUM);  
  tableau = malloc (4*taille*sizeof(char));
  index = 0;

  for(index = 0; index<taille; index++)
    tableau[2*index] = index;

  return 0;
}
